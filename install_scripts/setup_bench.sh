#!/bin/bash

set -e


## Utils

print_msg() {
	echo "Frappe password: $FRAPPE_USER_PASS"
	echo "MariaDB root password: $MSQ_PASS"
	echo "Administrator password: $ADMIN_PASS"
}

get_passwd() {
	echo `cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1`
}

get_distro() {
	ARCH=$(uname -m | sed 's/x86_/amd/;s/i[3-6]86/x86/') 

	if [ $ARCH == "amd64" ]; then
		T_ARCH="x86_64"
		WK_ARCH="amd64"
	else
		T_ARCH="i386"
		WK_ARCH="i386"
	fi 

	if [ -f /etc/redhat-release ]; then
		OS="centos"
		OS_VER=`cat /etc/redhat-release | sed 's/Linux\ //g' | cut -d" " -f3 | cut -d. -f1`

	elif [ -f /etc/lsb-release ]; then
		. /etc/lsb-release
		OS=$DISTRIB_ID
		OS_VER=$DISTRIB_CODENAME

	elif [ -f /etc/debian_version ]; then
		. /etc/os-release
		OS="debian"  # XXX or Ubuntu??
		OS_VER=$VERSION_ID
	fi

        if [ $OS == "LinuxMint" ]; then
                OS="Ubuntu"
                if [ $OS_VER == "rosa" ]; then
                    OS_VER="trusty"
                fi
        fi

	export OS=$OS
	export OS_VER=$OS_VER
	export ARCH=$ARCH
	export T_ARCH=$T_ARCH
	export WK_ARCH=$WK_ARCH
	echo Installing for $OS $OS_VER $ARCH 
}

run_cmd() {
	if $VERBOSE; then
		"$@"
	else
		# $@ 
		"$@" > /tmp/cmdoutput.txt 2>&1 || (cat /tmp/cmdoutput.txt && exit 1)
	fi
}

## add repos

add_centos_postgres_repo() {
	if [ $OS_VER == "6" ]; then
		if [ $WK_ARCH == "i386" ]; then
			run_cmd sudo rpm -Uvh http://yum.postgresql.org/9.4/redhat/rhel-6-i386/pgdg-centos94-9.4-3.noarch.rpm
		else
			run_cmd sudo rpm -Uvh http://yum.postgresql.org/9.4/redhat/rhel-6-x86_64/pgdg-centos94-9.4-3.noarch.rpm
		fi
	elif [ $OS_VER == "7" ]; then
		run_cmd sudo rpm -Uvh http://download.postgresql.org/pub/repos/yum/9.4/redhat/rhel-7-x86_64/pgdg-centos94-9.4-3.noarch.rpm
	fi		
}
 
add_ubuntu_postgres_repo() {
	run_cmd sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ '$OS_VER'-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
	run_cmd sudo apt-get -y install wget ca-certificates
	run_cmd wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
	run_cmd sudo apt-get -y update
	run_cmd sudo apt-get -y upgrade
}

add_ius_repo() {
	if [ $OS_VER -eq "6" ]; then
	wget http://dl.iuscommunity.org/pub/ius/stable/CentOS/$OS_VER/$T_ARCH/epel-release-6-5.noarch.rpm
	wget http://dl.iuscommunity.org/pub/ius/stable/CentOS/$OS_VER/$T_ARCH/ius-release-1.0-13.ius.centos6.noarch.rpm
	rpm --quiet -q epel-release || rpm -Uvh epel-release-6-5.noarch.rpm
	rpm --quiet -q ius-release || rpm -Uvh ius-release-1.0-13.ius.centos6.noarch.rpm
	fi
}

add_epel_centos7() {
	yum install -y epel-release
}

install_postgres() {
	if [ "$OS" == "Ubuntu" ] && [ $OS_VER == "utopic" ]; then
		return
	elif [ "$OS" == "centos" ]; then
		echo Adding centos postgres repo
		run_cmd add_centos_postgres_repo
		run_cmd yum install postgresql94-server postgresql94-contrib postgresql94-devel -y
		if [ $OS_VER == "6" ]; then 
			run_cmd sudo service postgresql-9.4 initdb
		elif [ $OS_VER == "7" ]; then
			run_cmd sudo /usr/pgsql-9.4/bin/postgresql94-setup initdb
		fi
		export PATH=$PATH:/usr/pgsql-9.4/bin
	
	elif [ "$OS" == "debian" ]; then 
		echo Adding debian postgres repo
		add_ubuntu_postgres_repo
		run_cmd sudo apt-get -y install postgresql-9.4
		run_cmd sudo apt-get -y install postgresql-contrib libpq-dev
	
	elif [ "$OS" == "Ubuntu" ]; then 
		echo Adding ubuntu postgres repo
		add_ubuntu_postgres_repo
		run_cmd sudo apt-get -y install postgresql-9.4
		run_cmd sudo apt-get -y install postgresql-contrib libpq-dev
	else
		echo Unsupported Distribution
		exit 1
	fi
}

## install 

install_packages() {
	if [ $OS == "centos" ]; then
		run_cmd sudo yum install wget -y
		run_cmd sudo yum groupinstall -y "Development tools"
		install_git_centos
		if [ $OS_VER == "6" ]; then 
			run_cmd add_ius_repo
			run_cmd sudo yum install -y python-pip ntp nginx freetype freetype-devel libpng-devel libjpeg-devel bzip2-devel python27-devel python27 libxml2 libxml2-devel libxslt libxslt-devel libXrender libXext python27-setuptools cronie sudo which libevent-devel ghostscript xorg-x11-fonts-Type1 xorg-x11-fonts-75dpi openldap-devel c-ares-devel libxmlsec1-devel libffi libffi-devel libtool-ltdl-devel
		elif [ $OS_VER == "7" ]; then
			run_cmd add_epel_centos7
			run_cmd sudo yum install -y python-pip ntp nginx freetype freetype-devel libpng-devel libjpeg-devel bzip2-devel python-devel libxml2 libxml2-devel libxslt libxslt-devel libXrender libXext cronie sudo which libevent-devel ghostscript xorg-x11-fonts-Type1 xorg-x11-fonts-75dpi openldap-devel c-ares-devel xmlsec1-devel libffi libffi-devel xmlsec1-openssl-devel libtool-ltdl-devel
		fi
		echo "Installing wkhtmltopdf"
		install_wkhtmltopdf_centos
		run_cmd easy_install-2.7 -U pip
	
	
	elif [ $OS == "debian" ] || [ $OS == "Ubuntu" ]; then 
		run_cmd sudo apt-get -y install python-pip python-dev git bzr ntp htop libxslt1.1 libxslt1-dev nginx libjpeg-dev libxrender1 libxext6 xfonts-75dpi xfonts-base ghostscript fontconfig libfreetype6-dev libxft-dev libldap2-dev libsasl2-dev libc-ares-dev libxmlsec1-dev libltdl3-dev openssl xsltproc
		echo "Updating xmlsec"
		run_cmd wget -O /dev/stdout http://www.aleksey.com/xmlsec/download/xmlsec1-1.2.20.tar.gz --no-check-certificate  | tar xzv
		cd xmlsec1-1.2.20 && ./configure && make && sudo make install && sudo ldconfig && cd ..
		echo "Installing wkhtmltopdf"
		install_wkhtmltopdf_deb

	else
		echo Unsupported Distribution
		exit 1
	fi

	echo "Installing virutalenv"
	run_cmd sudo pip install virtualenv
}

install_git_centos() {
# Installing Git 2.0 from source
	run_cmd sudo yum install curl-devel expat-devel gettext-devel openssl-devel zlib-devel -y
	run_cmd sudo yum install gcc perl-ExtUtils-MakeMaker -y
	run_cmd sudo yum remove git -y
	run_cmd cd /usr/src
	run_cmd sudo wget https://www.kernel.org/pub/software/scm/git/git-2.0.4.tar.gz
	run_cmd sudo tar xzf git-2.0.4.tar.gz
	run_cmd cd git-2.0.4
	run_cmd sudo make prefix=/usr/local/git all
	run_cmd sudo make prefix=/usr/local/git install
	run_cmd sudo echo "export PATH=$PATH:/usr/local/git/bin" >> /etc/bashrc
	run_cmd source /etc/bashrc
}

install_wkhtmltopdf_centos () {

	if [[ $OS == "centos" && $OS_VER == "7" && $T_ARCH == "i386" ]]; then
		echo "Cannot install wkhtmltodpdf. Skipping..."
		return 0
	fi
	RPM="wkhtmltox-0.12.2.1_linux-$OS$OS_VER-$WK_ARCH.rpm"
	run_cmd wget http://download.gna.org/wkhtmltopdf/0.12/0.12.2.1/$RPM
	rpm --quiet -q wkhtmltox || run_cmd rpm -Uvh $RPM
}

install_wkhtmltopdf_deb () {
	if [[ $OS_VER == "utopic" ]]; then
		echo "Cannot install wkhtmltodpdf. Skipping..."
		return 0
	fi
	if [[ $OS == "debian" &&  $OS_VER == "7" ]]; then
		WK_VER="wheezy"
	else
		WK_VER=$OS_VER
	fi
	run_cmd cd /tmp/
	run_cmd wget http://download.gna.org/wkhtmltopdf/0.12/0.12.1/wkhtmltox-0.12.1_linux-$WK_VER-$WK_ARCH.deb
	run_cmd sudo dpkg -i /tmp/wkhtmltox-0.12.1_linux-$WK_VER-$WK_ARCH.deb
}


### config

get_password() {
	if [ -z "$2" ]; then
		read -t 1 -n 10000 discard  || true
		echo
		read -p "Enter $1 password to set:" -s TMP_PASS1
		echo
		read -p "Re enter $1 password to set:" -s TMP_PASS2
		echo
		if [ $TMP_PASS1 == $TMP_PASS2 ]; then
			export $2=$TMP_PASS1
		else
			echo Passwords do not match
			get_password $1 $2
		fi
	fi
}

start_services_centos6() {
	run_cmd service nginx start
	run_cmd service postgresql start
	run_cmd service ntp start
}

configure_services_centos6() {
	run_cmd chkconfig redis on
	run_cmd chkconfig --levels 235 postgresql-9.4 on
	run_cmd chkconfig nginx on
	run_cmd chkconfig ntp on
}

configure_services_centos7() {
	run_cmd systemctl enable nginx
	run_cmd systemctl enable ntpd
	run_cmd systemctl enable postgresql-9.4
}

start_services_centos7() {
	run_cmd systemctl start nginx
	run_cmd systemctl start ntpd
	run_cmd systemctl start postgresql-9.4
}

install_bench() {
	run_cmd sudo su $FRAPPE_USER -c "cd /home/$FRAPPE_USER && git clone https://github.com/frappe/bench --branch $BENCH_BRANCH bench-repo"
	if hash pip-2.7 &> /dev/null; then
		PIP="pip-2.7"
	elif hash pip2.7 &> /dev/null; then
		PIP="pip2.7"
	elif hash pip2 &> /dev/null; then
		PIP="pip2"
	elif hash pip &> /dev/null; then
		PIP="pip"
	else
		echo PIP not installed
		exit 1
	fi
	run_cmd sudo $PIP install -e /home/$FRAPPE_USER/bench-repo
}

setup_bench() {
	echo Installing frappe-bench
	FRAPPE_BRANCH="develop"
	ERPNEXT_APPS_JSON="https://raw.githubusercontent.com/frappe/bench/master/install_scripts/erpnext-apps.json"
	if $SETUP_PROD; then
		FRAPPE_BRANCH="master"
		ERPNEXT_APPS_JSON="https://raw.githubusercontent.com/frappe/bench/master/install_scripts/erpnext-apps-master.json"
	fi
		
	run_cmd sudo su $FRAPPE_USER -c "cd /home/$FRAPPE_USER && bench init frappe-bench --frappe-branch $FRAPPE_BRANCH --apps_path $ERPNEXT_APPS_JSON"
	echo Setting up first site
	echo /home/$FRAPPE_USER/frappe-bench > /etc/frappe_bench_dir
	run_cmd sudo su $FRAPPE_USER -c "cd /home/$FRAPPE_USER/frappe-bench && bench new-site site1.local --mariadb-root-password $MSQ_PASS --admin-password $ADMIN_PASS"
	run_cmd sudo su $FRAPPE_USER -c "cd /home/$FRAPPE_USER/frappe-bench && bench frappe --install_app erpnext"
	run_cmd sudo su $FRAPPE_USER -c "cd /home/$FRAPPE_USER/frappe-bench && bench frappe --install_app shopping_cart"
	run_cmd bash -c "cd /home/$FRAPPE_USER/frappe-bench && bench setup sudoers $FRAPPE_USER"
	if $SETUP_PROD; then
		run_cmd bash -c "cd /home/$FRAPPE_USER/frappe-bench && bench setup production $FRAPPE_USER"
	fi
}

add_user() {
	export OPENERP_USER="openerp"
	export OPENERP_USER_PASS="openerp"

	USER_EXISTS=`bash -c "id $OPENERP_USER > /dev/null 2>&1  && echo true || (echo false && exit 0)"`

	if [ $USER_EXISTS == "false" ]; then
		useradd -m -d /home/$OPENERP_USER -s $SHELL $OPENERP_USER
		echo $OPENERP_USER:$OPENERP_USER_PASS | chpasswd
		chmod o+x /home/$OPENERP_USER
		chmod o+r /home/$OPENERP_USER
	fi

	# Add user role into postgresql configuration for allow create databases
	run_cmd sudo su - postgres -c "createuser  --superuser $OPENERP_USER"

}

main() {
	get_distro
	install_postgres	
	echo Installing packages for $OS\. This might take time...
	install_packages
	if [ $OS == "centos" ]; then
		if [ $OS_VER == "6" ]; then
			echo "Configuring CentOS services"
			configure_services_centos6
			echo "Starting services"
			start_services_centos6
		elif [ $OS_VER == "7" ]; then
			echo "Configuring CentOS services"
			configure_services_centos7
			echo "Starting services"
			start_services_centos7
		fi
	fi
	echo "Adding openerp user"
	add_user
#	install_bench
#	if $SETUP_BENCH; then
#		setup_bench
#	fi
#
#	echo
#	RUNNING=""
#	if $SETUP_PROD; then
#		RUNNING=" and is running on port 80"
#	fi
#	echo "Frappe/ERPNext is installed successfully$RUNNING."
#	print_msg > ~/frappe_passwords.txt
#	print_msg
}

main $@
