# The PyERP Buildout


This is a standard tool to create and configure PyERP instances of the company `OpenPyme <http://www.openpyme.mx>`.

## Contents

* [Main features](#main-features)
* [Structure](#structure)
* [Quickstart](#quickstart)
* [Configuring a project](#configuring-a-project)
* [Use in development](#use-in-development)
* [Use in production](#use-in-production)
* [Use for test-instances](#use-for-test-instances)
* [Features](#features)
  * [Debugging and Development](#debugging-and-development)
  * [Testing](#testing)
  * [Production](#production)
* [Notes](#notes)

## Main features


* It extends to config- and version-files on shared repo by all projects that use the same version of PyERP.
* It allows to update a project simply by changing the version it extends.
* It allows to update all projects of one version by changing remote files (very useful for HotFixes).
* It is minimal work to setup a new project.
* It has presets for development, testing, staging and production.
* It has all the nice development-helpers we use.


## Structure

``buildout.cfg``
    This contains the project settings (name, addons, checkouts etc.).

``local.cfg``
    For each environment (development, production, test) there is a separate ``profiles/*.cfg``-file. You create a *symlink*  called ``local.cfg`` to one of these files depending on your environment. Each of the files includes the ``base.cfg``  like this:


        extends = config/base.cfg

``base.cfg``
    This file conatains most of the commonly used logic used for deploy PyERP software. It also includes two version-files that are also hosted on remote repo:

    * `pinned_versions.cfg <config/pinned_versions.cfg>`_: Pinns the PyERP-version.
    * `floating_versions.cfg <config/floating_versions.cfg>`_: Pinns all commonly used addons of this buildout.

``pinned_versions_project.cfg``
    Here you pinn versions to overwrite or extend the hosted ``pinned_versions.cfg``. These eggs are usually pinned for a reason and are usually not safe to be upgraded.

``floating_versions_project.cfg``
    Here you overwrite and extend the hosted ``floating_versions.cfg``. These eggs should usually be safe to be upgraded. ``./bin/checkversions floating_versions_project.cfg`` will check pypi if there are newer releases for your pinned eggs.

## Quickstart


    $ git clone http://gitlab.openpyme.mx/pyerp/bench.git SOME_PROJECT

Symlink to the file that best fits you environment. At first that is usually production. Later you can use development or test.


    $ ln -s profiles/production.cfg local.cfg

Build PyERP


    $ virtualenv . --no-site-packages --no-setuptools
    $ bin/pip install zc.buildout
    $ bin/buildout


## Configuring a project

``buildout.cfg`` contains the general project settings. Here you configure the name of the project, the eggs, source-checkouts and options PyERP will use.


## Use in development

Symlink to the development-config:


    $ ln -s profiles/develop.cfg local.cfg

The development-setup will build a simple instance with some useful tools (see below). The setup assumes that supervisor, logrotate, postgresql and nginx are only configured on production.


## Use in production

Symlink to the production-config:


    $ ln -s profiles/production.cfg local.cfg


## Use for test-instances

Symlink to the t-config:


    $ ln -s profiles/production.cfg local.cfg


## Features

### Debugging and Development

**pyerp**

:    Run ``bin/start_openerp`` to start your PyERP instance.

**packages**

:    All modules of your PyERP instance will be under ``parts/`` folder.

**pydev**

:    A ``.pydevproject`` file will be created so you can start develop in no time by importing your new instance into Eclipse PyDev. All your modules will be symlinked so you can benefit from PyDev auto completion features. Configure the name of your project on ``buildout.cfg`` under site option.

**ipython**

:    Run ``bin/openerp_proxy`` to have a ipython-prompt with all eggs of your instance in its python-path to perform operations on PyERP objects remotely using XML-RPC or JSON-RPC. Check [here](https://github.com/katyukha/openerp-proxy/blob/master/README.rst) for a complete list of features and usage examples.

**checkversions**

:    Run ``bin/checkversions floating_versions_project.cfg`` to check if your pinned eggs are up-to-date.

**isort**

: Run ``bin/isort path/to/your/file/file.py`` to reorder your imports so it follow PyERP code style conventions.


### Testing

**green**

:    Run ``bin/green -d database_name -- path/to/module`` to run unit test for module on database.

**radish**

: Run ``bin/radish -d database_name -- path/to/module/tests/functional/features/ -b path/to/module/tests/functional/radish`` to reorder your imports so it follow PyERP code style conventions.


### Production

**service**

:    PyERP service will be configured to auto start on host reboot. Run ``bin/restart`` to restart PyERP service, and ``bin/status`` to know the current status of PyERP service.


**Sentry logging**

:    Configure PyERP to send tracebacks to Sentry by adding your dsn on ``buildout.cfg`` under openerp options.

**backups**

:    A backup job will be created, this backup job will run every night and backup all your databases on ``backup/latest`` folder. Other job will be run every day, week and moth to create a solid backup retention police on ``backup/rsnapshot``.

## Notes

``local.cfg`` must **never** be versioned. The file ``.gitignore`` in this buildout already prevent this.

It might feels weird that ``buildout.cfg`` loads ``local.cfg``, but this avoids some weird behavior of buildouts extends-feature.

