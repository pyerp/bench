<a name="2018.04.03"></a>
## 2018.04.03 (2018-05-12)


#### Bug Fixes

* **l10n_mx_toponyms:**  set version 1.0.3 ([75c04565](75c04565))
* **point_of_sale:**  set version 1.5.1 ([d09c7aba](d09c7aba))
* **pos_invoice_posted:**  set version 1.3.3 ([f9790aae](f9790aae))



<a name="2018.04.02"></a>
## 2018.04.02 (2018-04-27)


#### Bug Fixes

* **account:**  set version 1.5.3 ([85021764](85021764))
* **l10n_mx_validate_cfdi:**  set version 1.2.3 ([6b1b9f60](6b1b9f60))
* **manufacture:**  set version 1.3.1 ([c50e34bf](c50e34bf))
* **pos_invoice_posted:**  set version 1.3.2 ([0324d414](0324d414))



<a name="2018.04.01"></a>
## 2018.04.01 (2018-04-21)


#### Features

* **product:**  set versino 1.3.0 ([5eaa1e5a](5eaa1e5a))



<a name="2018.04.00"></a>
## 2018.04.00 (2018-04-20)


#### Bug Fixes

* **aeroo_reports:**  update to version 1.4.2 ([219a9bce](219a9bce))

#### Features

* **hr_employee_benefit:**  se agrega el módulo ([431da3eb](431da3eb))
* **l10n_mx_facturae:**  set version 2.5.0 ([10061ec0](10061ec0))
* **l10n_mx_facturae_cdetallista:**  set version 1.0.0 ([07798d9d](07798d9d))
* **mrp:**  set version 1.3.0 ([6749f4ae](6749f4ae))
* **num2words:**  actualiza módulo a la versión 0.5.7 ([479a1e7b](479a1e7b))
* **request:**  update dependences versions ([de8ad1d8](de8ad1d8))
* **sale_stock:**  set version 1.2.0 ([c10dae45](c10dae45))
* **stock:**  set version 1.8.0 ([d4728c52](d4728c52))



<a name="2018.03.05"></a>
## 2018.03.05 (2018-04-18)


#### Bug Fixes

* **aeroo_reports:**  set version 1.4.3 ([705d8f3e](705d8f3e))
* **ccount_financial_report_engine:**  set version 1.1.4 ([3d10299a](3d10299a))
* **l10n_mx_account_tax:**  set version 1.2.1 ([d38d5fdd](d38d5fdd))
* **l10n_mx_payroll:**  set version 1.3.4 ([b6671a79](b6671a79))
* **product:**  set version 1.2.1 ([d4e0607a](d4e0607a))
* **sale:**  set version 1.1.2 ([89cdde31](89cdde31))
* **stock:**  set version 1.7.3 ([50062237](50062237))



<a name="2018.03.04"></a>
## 2018.03.04 (2018-04-14)


#### Bug Fixes

* **account:**
  *  set version 1.5.2 ([9af720b8](9af720b8))
  *  set version 1.5.1 ([008a29ad](008a29ad))
* **account_financial_reporting_engine:**  set version 1.1.3 ([8d9f4623](8d9f4623))
* **l10n_mx_facturae:**  set version 2.4.9 ([62407108](62407108))
* **purchase:**  set version 1.2.1 ([c69ab14d](c69ab14d))
* **sale:**  set version 1.1.1 ([54422481](54422481))



<a name="2018.03.03"></a>
## 2018.03.03 (2018-04-04)


#### Bug Fixes

* **ea_import:**  set version 1.1.2 ([faa2740b](faa2740b))



<a name="2018.03.02"></a>
## 2018.03.02 (2018-04-04)


#### Bug Fixes

* **hr_contract:**  set version 1.2.1 ([28ec9a4d](28ec9a4d))



<a name="2018.03"></a>
## 2018.03 (2018-03-24)


#### Breaking Changes

* **aeroo_reports:**  pin module on version previus to 1.3.0 that breaks installation of reports on a ([d2604951](d2604951))

#### Code refactor

* **base.cfg:**
  *  update modules in order to bring newer versions ([b57669de](b57669de))
  *  update aeroo_report module ([1089ca89](1089ca89))
  *  update l10n_mx_account_tax module to 1.0.10 version ([34bf5f48](34bf5f48))
  *  update PyERP to 8.0.0.rc7 ([89389f72](89389f72))

#### Bug Fixes

*   change the link necessary for install pyxml ([d5d95bf9](d5d95bf9))
* **PyERP and facturacion version:**  change master version of these repositories ([89ff8514](89ff8514))
* **account:**
  *  set version 1.5.0 ([38f21166](38f21166))
  *  set version 1.4.2 ([d869bdcb](d869bdcb))
  *  set version 1.4.1 ([c2e85440](c2e85440))
  *  update to version 1.1.12 ([68e8582c](68e8582c))
  *  update account module to version 1.2.0 ([29eda00e](29eda00e))
* **account_check_writting:**  set version 1.1.4 ([dbacc9ca](dbacc9ca))
* **account_financial_report_engine:**  set version 1.1.2 ([809e0f3b](809e0f3b))
* **account_financial_reporting:**  set version 1.1.1 ([5e0c015c](5e0c015c))
* **account_finantial_reporting:**  update account_finantial_reporting module ([ae92424d](ae92424d))
* **account_followup:**  set version 1.0.1 ([25dcba07](25dcba07))
* **account_payment_type:**
  *  set version 1.4.3 ([0f3b146c](0f3b146c))
  *  set version 1.4.2 ([494ac5a6](494ac5a6))
  *  update to version 1.4.1 ([06e67724](06e67724))
  *  update account_payment_type module ([9f1e9c72](9f1e9c72))
* **account_petty_cash:**
  *  set version 1.2.2 ([5fcf1742](5fcf1742))
  *  update to version 1.2.1 ([d36f6ae0](d36f6ae0))
* **account_voucher:**
  *  set version 1.3.4 ([de2838d9](de2838d9))
  *  set version 1.3.3 ([d42eb44e](d42eb44e))
  *  set version 1.3.2 ([6856ffb4](6856ffb4))
  *  set version 1.3.1 ([ec71a1df](ec71a1df))
* **aeroo_reports:**
  *  set version 1.4.1 ([1d3a0e7e](1d3a0e7e))
  *  update to version 1.3.2 ([e9c49c84](e9c49c84))
  *  pin module on version previus to 1.3.0 that breaks installation of reports on a ([d2604951](d2604951))
* **aeroolib:**  fija el commit que se debe utilizar del aeroolib ([2e16333a](2e16333a), closes [#38](38))
* **appdirs:**  add new depedencies eggs for setuptools ([858f8c37](858f8c37))
* **base:**
  *  update account module to 1.1.7 v ([38721409](38721409))
  *  Replace obsolet accounting-addons namespace ([9cfe9cdc](9cfe9cdc))
  *  Move facturacion repo to its own folder and stop using l10n_mx ([c5d065fc](c5d065fc))
* **base.cfg:**
  *  prevent error where buidout delete main project ([57d9220c](57d9220c), closes [#31](31))
  *  use ; instead of # for comments ([b47ef72d](b47ef72d))
  *  postgres centos ([3ecc8d2c](3ecc8d2c))
  *  Set versions for several modules ([1e584f20](1e584f20))
* **base_pyerp:**
  *  set version 1.1.3 ([e54e1e9b](e54e1e9b))
  *  set version 1.1.2 ([4f92bc37](4f92bc37))
  *  set version 1.1.1 ([6a94f584](6a94f584))
* **buildout:**
  *  remove auth_signup 1.0.0 ([b154ab7a](b154ab7a))
  *  set default value for limit_memory_hard param ([7d3a4286](7d3a4286))
  *  set default value for db_maxconn for a more realistic value ([7124dbe7](7124dbe7))
* **centos7:**  add missing libraries for compilation ([b37e6185](b37e6185))
* **community-data:**  fix bad URL for module ([6cdbaece](6cdbaece))
* **contract:**  update to version 2.1.1 ([f771986a](f771986a))
* **crm:**  set version 1.0.1 ([5d8b0589](5d8b0589))
* **crm_claim:**  update to version 1.0.0 ([b56fbfe6](b56fbfe6))
* **cryptography:**  set version 2.1.3 ([e9d120b9](e9d120b9))
* **debtcollector:**
  *  set verstion 1.17.1 ([1af6b7dd](1af6b7dd))
  *  update to version 1.15.0 ([fb18df56](fb18df56))
* **develop:**
  *  Pinn pyinotify egg ([ddd8d3ca](ddd8d3ca))
  *  Add missing parameter on mrbob part ([cb11205b](cb11205b))
* **ea_import:**  set version 1.1.1 ([cf2b6ecb](cf2b6ecb))
* **eggs:**  update versions for eggs needed for install PyERP ([0a4bd114](0a4bd114))
* **elaphe3:**  set proper version on versions manifest ([ea0af907](ea0af907))
* **facturacion:**
  *  set version 4.0.2 ([f87887ec](f87887ec))
  *  set version 4.0.1 ([1ac563d1](1ac563d1))
* **facturacion module:**  update facturacion module in order to bring patches ([213c7688](213c7688))
* **fleet:**  update to version 0.1.1 ([6947d881](6947d881))
* **floating:**  remove ununsed modules ([ab1368e1](ab1368e1))
* **gitignore:**  Add more unecesary files to being exclude from git ([b712b43e](b712b43e))
* **hr_attendance:**  update to version 1.1.1 ([fd60a566](fd60a566))
* **hr_expense:**  update hr_expense module ([5fad88a0](5fad88a0))
* **hr_recuitment:**  set version 1.0.1 ([8efd73ff](8efd73ff))
* **install_scripts.sh:**  update postgres url ([fd4c8301](fd4c8301))
* **ipython:**  update to version 5.4.0 ([50d1e5f6](50d1e5f6))
* **isort:**  update to version 4.2.15 ([1f589881](1f589881))
* **l10n_mx:**
  *  update to version 2.3.4 ([85f67236](85f67236))
  *  set version 2.3.3 ([54c2b509](54c2b509))
  *  set version 2.3.2 ([1967c2f5](1967c2f5))
  *  update to version 2.3.1 ([778a9c30](778a9c30))
* **l10n_mx_account_bank_statement_import:**
  *  set version 0.5.0 ([de11ac4d](de11ac4d))
  *  update to version 0.4.1 ([38517da7](38517da7))
* **l10n_mx_account_tax:**
  *  set version 1.2.0 ([f111b244](f111b244))
  *  update to version 1.1.1 ([79aebade](79aebade))
  *  update to version 1.0.11 ([dbeac817](dbeac817))
* **l10n_mx_diot:**  update group ([81ca5025](81ca5025))
* **l10n_mx_eaccounting:**
  *  update to version 1.1.2 ([9544c5c1](9544c5c1))
  *  update to version 1.1.1 ([e33ef75c](e33ef75c))
* **l10n_mx_facturae:**
  *  set version 2.4.8 ([35461d64](35461d64))
  *  set version 2.4.7 ([6a9c7236](6a9c7236))
  *  set version 2.4.6 ([06a9a616](06a9a616))
  *  set version 2.4.5 ([2f3f7b66](2f3f7b66))
  *  set version 2.4.4 ([11c7136c](11c7136c))
  *  set version 2.4.3 ([cd185b3a](cd185b3a))
  *  set version 2.4.2 ([1f0b602d](1f0b602d))
* **l10n_mx_facturae_validate_cfdi:**
  *  set version 1.2.2 ([73930a5d](73930a5d))
  *  set version 1.2.1 ([e2a4cebe](e2a4cebe))
  *  update to version 1.2.0 ([e1244bbf](e1244bbf))
* **l10n_mx_ir_attachment_facturae:**  set version 2.0.3 ([7a94073a](7a94073a))
* **l10n_mx_payment_method:**  remove deprecated module ([ae4d90fb](ae4d90fb))
* **l10n_mx_payroll:**
  *  set version 1.3.3 ([61e0807d](61e0807d))
  *  set version 1.3.2 ([c90eec5e](c90eec5e))
  *  set version 1.3.1 ([25948132](25948132))
  *  set version 1.3.0 ([acacc177](acacc177))
  *  update to version 1.2.3 ([f5aedf10](f5aedf10))
* **l10n_mx_toponyms:**
  *  set version 1.0.2 ([ae9f0a0b](ae9f0a0b))
  *  set version 1.0.1 ([f2585c2c](f2585c2c))
* **libxmlsec:**  install before openerp libxmlsec ([2778447c](2778447c))
* **mail:**
  *  set version 1.1.2 ([e83abcd4](e83abcd4))
  *  set version 1.1.1 ([563d57ab](563d57ab))
  *  set version 1.0.7 ([b2cee8a1](b2cee8a1))
  *  update to version 1.0.6 ([0a04e441](0a04e441))
* **nginx.tpl:**  Nginx correction to correctly identify remote ip addresses ([d1d57dba](d1d57dba))
* **odoo:**  update to version 8.0.0.rc9 ([1e7dc80d](1e7dc80d))
* **options:**  update default values for memory limits ([dbe23864](dbe23864))
* **pbr:**  update to version 2.0.0 ([681368d0](681368d0))
* **point_of_sale:**  update to version 1.3.2 ([283c65d0](283c65d0))
* **pos_invoice_posted:**  set version 1.3.1 ([c72db9de](c72db9de))
* **postgres in centos 7:**  update postgres repository in centos 7 ([e704f97d](e704f97d))
* **postgresql:**  fix bin path ([d6450b79](d6450b79))
* **product:**
  *  set version 1.2.0 ([c7656648](c7656648))
  *  get back to version 1.1.3 ([43738979](43738979))
  *  set version 1.1.4 ([5d742aef](5d742aef))
  *  set version 1.1.3 ([99cb68b5](99cb68b5))
  *  set version 1.1.2 ([71abcf49](71abcf49))
* **production:**  add eggs needed for raven ([1f26469e](1f26469e))
* **project_dirs:**  add missing folders for being auto created ([d99b1a73](d99b1a73))
* **psycopg2:**
  *  update to version 2.7.3.2 ([3a663655](3a663655))
  *  update to version 2.7.1 ([7064cc66](7064cc66))
* **purchase:**
  *  set version 1.2.0 ([071682dd](071682dd))
  *  set version 1.1.2 ([3249efe7](3249efe7))
  *  set version 1.1.1 ([4edb7415](4edb7415))
* **purchase_landed_cost:**  set version 2.5.2 ([be95beca](be95beca))
* **purchase_request:**  use proper branch for module purchase_request ([4abd0300](4abd0300))
* **purchase_requisition:**  set version 1.1.2 ([0cf55fb9](0cf55fb9))
* **pychart:**
  *  remove death download.gna.org service ([625a8ab3](625a8ab3))
  *  remove deprecated link to download.gna.org ([b7ccbcf0](b7ccbcf0))
* **pyusb:**  update to version 1.0.2 ([f6e71571](f6e71571))
* **pyxmlsec:**  update pyxmlsec to a version able to install on setuptools > 34.0.0 ([ceea4c1b](ceea4c1b))
* **reporting-engine:**  update repo to version 8.0 ([cca65251](cca65251))
* **res.company:**  update to version 1.2.1 ([767d4a77](767d4a77))
* **sale:**
  *  set version 1.1.0 ([24281aa0](24281aa0))
  *  set version 1.0.1 ([f8d6db81](f8d6db81))
* **sale_stock:**
  *  set version 1.1.1 ([dbfa67fe](dbfa67fe))
  *  set version 1.1.1 ([142ab6f3](142ab6f3))
* **sentry:**  update module to version 1.2.1 ([ba6fb1b6](ba6fb1b6))
* **server:**
  *  set version 8.0.0.rc12 ([6330548a](6330548a))
  *  set version 8.0.0.rc11 ([72066ea6](72066ea6))
  *  set version 8.0.0.rc10 ([29f30d14](29f30d14))
  *  update to version 8.0.0.rc.8 ([93d2c959](93d2c959))
* **stock:**
  *  set version 1.7.2 ([a4f25b20](a4f25b20))
  *  update to version 1.7.1 ([df099959](df099959))
* **typing:**  set typing version ([6f303a3c](6f303a3c))
* **versions:**  Pinn wheel to version 0.23.0 in order to satisfy openupgradelib dependencies ([fa312075](fa312075))
* **warning:**  set version 1.0.1 ([8e1c27bb](8e1c27bb))
* **zc.buildout:**  use zc.buildout version available on environment ([a22f2925](a22f2925))

#### Features

* **M2Crypto:**  add egg ([0d698115](0d698115))
* **account:**  set to version 1.4.0 ([29ae8235](29ae8235))
* **account_analytic_analysis_recurring:**  add module account_analytic_analysis_recurring for repeat ([32ed1f64](32ed1f64))
* **account_anglo_saxon:**  add module version 1.2.0 ([fe7eb366](fe7eb366))
* **account_invoice_discount:**  add module ([9c69edf5](9c69edf5))
* **account_invoice_merge:**  add module version 2.0.1 ([cdce01a1](cdce01a1))
* **account_payment_type:**  set to version 1.4.0 ([38950e04](38950e04))
* **account_petty_cash:**  add module account_petty_cash ([3426b2c9](3426b2c9))
* **addons:**  update addons to march releases ([0d9203be](0d9203be))
* **aeroo_reports:**  update to version 1.4.0 ([78000250](78000250), closes [#22](22))
* **backup:**  Improve backup script assuming postgresql commands are on PATH ([a8ac2c86](a8ac2c86))
* **base:**  Update stock module to version 1.4.0 ([88c73661](88c73661))
* **base.cfg:**  udapte modules to last release ([946d0f9b](946d0f9b))
* **base_pyerp:**  set version 1.1.0 ([4bac94ad](4bac94ad))
* **board:**  add board module ([ccc24c84](ccc24c84))
* **buildout.cfg:**  add support for data_dir option for odoo v8.0 or later ([f35a7a57](f35a7a57))
* **community_data:**  add community data repo ([c25b1efa](c25b1efa))
* **connector:**  add connector module for defer jobs ([a6de4ee9](a6de4ee9))
* **connector-interfaces:**  add new module connector-interfaces ([e8ce6f49](e8ce6f49))
* **crm:**  add module crm ([2a091c04](2a091c04))
* **develop:**
  *  Replace paster with bob ([5f61079b](5f61079b))
  *  Add pynotify on develop mode in order to be able to add --auto-reload to openerp ([03ac49da](03ac49da))
* **elaphe:**  replace elaphe with elaphe3 library ([8b417dd9](8b417dd9))
* **facturacion:**  set version 4.0.0 ([93d4abea](93d4abea))
* **hr_attendance:**
  *  set version 1.2.0 ([e173983e](e173983e))
  *  add module hr_attendance ([07c5edec](07c5edec))
* **hr_payroll:**
  *  set version 1.4.0 ([174bb84b](174bb84b))
  *  Update module to version 1.1.0 ([b9fb49ea](b9fb49ea))
* **ht_timesheet_sheet:**  add version 1.0.0 ([bebcad33](bebcad33))
* **l10n_mx:**
  *  update to version 2.3.0 ([0a1cc381](0a1cc381))
  *  Add some missing modules for l10n_mx ([7fd4e813](7fd4e813))
* **l10n_mx_account_tax:**  update to version 1.1.0 ([313bb00a](313bb00a))
* **l10n_mx_facturae:**  set version 2.4.0 ([25ea9238](25ea9238))
* **l10n_mx_facturae_cexterior:**  update module to version 1.0.0 ([31712a2b](31712a2b))
* **l10n_mx_facturae_validate_cfdi:**  update to version 1.1.0 ([6ef575ec](6ef575ec))
* **l10n_mx_toponyms:**  update to version 1.0.0 ([da91954c](da91954c))
* **mail:**  set version 1.1.0 ([e01a2b54](e01a2b54))
* **mrp_operations:**  add module ([50f5c3b3](50f5c3b3))
* **multicompany:**  new config base that allow to holds modules for configure an instance as a multi ([67587767](67587767))
* **nginx:**  Add templates for nginx config ([98ed8acf](98ed8acf))
* **openerp:**  Add aeroo_reports and base_pyerp modules ([dea3dc5d](dea3dc5d))
* **openerp_proxy:**  set version 0.7.1 ([339cbc31](339cbc31))
* **openssl and xsltproc:**  add automatic installation for openssl and xsltproc ([24655aed](24655aed))
* **partner-contact:**  add partner-contact ([71c5c9dd](71c5c9dd))
* **passwd:**  seta stronger passwd by default ([b5e65ba8](b5e65ba8))
* **point_of_sale:**  set version 1.4.0 ([6689e345](6689e345))
* **pos_invoice_posted:**  set version 1.3.0 ([5ae0c993](5ae0c993))
* **product:**  add product repo ([ee691888](ee691888))
* **product.variant:**  add repo for product-variant modules ([37eb7bab](37eb7bab))
* **production:**  Update sentry dependencies ([490dd103](490dd103))
* **project:**  add module project ([10b93cea](10b93cea))
* **project_issue:**  add module ([190670ec](190670ec))
* **project_timesheet:**  add module project_timesheet ([f5293c6d](f5293c6d))
* **purchase:**  add module purchase ([0634ade4](0634ade4))
* **purchase_request:**  add repor for module purchase_request ([7fab478d](7fab478d))
* **python packages versions:**  update python packages versions ([55629e72](55629e72))
* **releaser:**  add releaser in order to make simple make project releases ([331e27b1](331e27b1))
* **sale_crm:**  add module sale_crm ([23b91592](23b91592))
* **sale_stock:**  update module to version 1.1.0 ([883abe65](883abe65))
* **sentry:**  set sentry key by default ([be6c9216](be6c9216))
* **server-tools:**  add server-tools repo from OCA to base configuration ([5dd0a580](5dd0a580))
* **template backup:**  filestore backup ([83de32d3](83de32d3))
* **test:**  add scripts for run unit test and bussiness driven development test ([295421fb](295421fb))
* **veresions.cfg:**  Normalize versions file ([8115ebd1](8115ebd1))
* **versions:**
  *  update dependencies to latest versions ([772f9499](772f9499))
  *  Pinn version for elaphe and aeroolib needed for aeroo_reports to work properly ([9a99e229](9a99e229))



