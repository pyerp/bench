upstream odoo {
    server localhost:{options:xmlrpc_port} weight=1 fail_timeout=3000s;
}

server {
    listen 80;
    listen [::]:80 ipv6only=on;
    server_name ${buildout:server_name};

    # Specifies the maximum accepted body size of a client request,
    # as indicated by the request header Content-Length.
    client_max_body_size        200m;

    # Hide nGinx version as security best practice
    server_tokens               off;

    # add ssl specific settings
    keepalive_timeout           60;

    # increase proxy buffer to handle some OpenERP web requests
    proxy_buffers               16 64k;
    proxy_buffer_size           128k;

    location / {
        proxy_pass              http://odoo;

        # Force timeouts if the backend dies
        proxy_next_upstream error timeout invalid_header http_500 http_502 http_503;

        add_header X-Static no;

        # Set headers
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        # Set timeouts
        proxy_connect_timeout   3600;
        proxy_send_timeout      3600;
        proxy_read_timeout      6h;
        send_timeout            3600;

        # By default, do not forward anything
        proxy_redirect          off;

        # enable data compression
        gzip                    on;
        gzip_min_length         1100;
        gzip_buffers            4 32k;
        gzip_types              text/plain application/x-javascript text/xml text/css;
        gzip_vary               on;
    }

    # Cache some static data in memory for 60mins.
    # under heavy load this should relieve stress on the Odoo web interface a bit.
    location ~* /[0-9a-zA-Z_]*/static/ {
        proxy_cache_valid       200 60m;
        proxy_buffering         on;
        expires                 864000;
        proxy_pass              http://odoo;
        # Turn off log for files
        log_not_found           off;
        access_log              off;
    }

    access_log /var/log/nginx/${buildout:site}.access.log;
    error_log  /var/log/nginx/${buildout:site}.error.log;
}
