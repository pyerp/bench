#!/bin/bash
# This script is public domain. Feel free to use or modify as you like.
BZIP2="/bin/bzip2"
GREP="/bin/grep"
USER="${env:USER}"
DUMPALL="${postgresql:location}pg_dumpall"
PGDUMP="${postgresql:location}pg_dump"
PSQL="${postgresql:location}psql"
DATE="$(date +%d-%m-%Y)"

# directory to save backups in, must be rwx by postgres user
BACKUPDIR="${backup:directory}"

# Filestore directory
FILESTOREDIR="${buildout:filestore}"

# Make sure the backup directory is cleaned before re-generate the new backups
rm $BACKUPDIR/*

# get list of databases in system for current user
# command inspired on SISalp suggestion on odoo mail list 
# https://www.odoo.com/groups/community-59/community-15954813
DBS=`$PSQL -l | $GREP " $USER " | cut -d"|" -f1`

# now backup the tables
for DB in $DBS; do
    # It would have been nice to do the next using pipe
    # but pipe didnt now allow me to redirect pg_dump output to input tar
    # at least I couldn't ;(
    cd /tmp
    if [ -d "$FILESTOREDIR/filestore/$DB" ]; then
    	$PGDUMP $DB  > $DB.sql && tar -cjf $BACKUPDIR/$DB-$DATE.tar.bz2 $DB.sql  -C $FILESTOREDIR/ filestore/$DB && rm -rf $DB.sql
    else
    	$PGDUMP $DB | $BZIP2 > $BACKUPDIR/$DB-$DATE.bz2	
    fi
done

# rsnapshoting
rsnapshot -c ${rsnapshot:output} daily
