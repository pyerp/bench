Development profile
code-analysis
:    This installs a pre-commit-hook that runs the codeanalysis-tests from ``plone.recipe.codeanalysis``.

Testing profile
Setup for jenkins
:    Configure jenkins to run the script ``./bootstrap_jenkins.sh``. This will configure and run the whole buildout.

Production profile
nginx
:    TODO: Documentation

monitoring
:    Change the settings for ``maxram`` to have memmon restart an instance when it uses up to much memory.

backups
:    Create backup for one database on request.

